﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LVL2_ASPNet_MVC_03.Controllers
{
    public class EmployeeController : Controller
    {
        // GET: Employee
        EmployeeContext db = new EmployeeContext();
        public ActionResult Index()
        {
            return View(db.Employees.ToList());
        }
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(Employee employee)
        {
            using (var transactions = db.Database.BeginTransaction())
            {
                //--if (ModelState.IsValid)
                try
                {
                    var removeRecord = db.Employees.Where(x => x.Id == 2).FirstOrDefault();
                    db.Employees.Remove(removeRecord);
                    db.Employees.Add(employee);
                    db.SaveChanges();
                    transactions.Commit();
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    transactions.Rollback();
                    return RedirectToAction("Index");
                }
                return View(employee);
            }
            
        }
        
        public ActionResult Details(int Id)
        {
            return View(db.Employees.Where(x => x.Id ==Id).FirstOrDefault());
        }
        [HttpGet]
        public ActionResult Edit (int Id)
        {
            return View(db.Employees.Where(x => x.Id == Id).FirstOrDefault());
        }
        [HttpPost]
        public ActionResult Edit (int Id, Employee employee)
        {
                db.Entry(employee).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
        }
        public ActionResult Delete(int Id)
        {
            return View(db.Employees.Where(x => x.Id == Id).FirstOrDefault());
        }
        [HttpPost]
        public ActionResult Delete(int Id, FormCollection collection)
        {
            Employee employee = db.Employees.Where(x => x.Id == Id).FirstOrDefault();
            db.Employees.Remove(employee);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

    }
}